API ->
go to project directory,
    please create .env in root folder and paste from .env.example file
    (Please change your mongo db connection what you want in .env)
 and run 
    npm install
    npm start
    

then you can access 4 api,
    1.base_url/register
    2.base_url/login
    3.basue_url/profile  (need to pass JWT auth token in header)
    4.base_url/logut     (need to pass JWT auth token in header)

    Swagger Document configuerd please vist (http://localhost:3000/api-docs/) here you can access all API

    or
    
    curl File Are Here,

    1. Register API CURL

        curl --location --request POST 'localhost:3000/register' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "userName":"insaf2",
            "password":"123456",
            "fullName":"Insaf Zakariya"
        }'

    2. Login API CURL

        curl --location --request POST 'localhost:3000/login' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "userName":"insaf2",
            "password":"123456"
            
        }'
    3. Profile API CURL

        curl --location --request GET 'localhost:3000/profile' \
        --header 'Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmFmNWQ4ZmZjNzI2MjAzNjU0OTUwMTgiLCJpYXQiOjE2MDUzMjgzNjksImV4cCI6MTYwNTMzMTk2OX0.O8mQl9yHOa8FYqHxyqzfWN3sG8V6CTkjFMJz_V85SBw'

    4.  Logout API CURL

        curl --location --request POST 'localhost:3000/logout' \
        --header 'Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmFmNWQ4ZmZjNzI2MjAzNjU0OTUwMTgiLCJpYXQiOjE2MDUzMjgzNjksImV4cCI6MTYwNTMzMTk2OX0.O8mQl9yHOa8FYqHxyqzfWN3sG8V6CTkjFMJz_V85SBw'

Test Cases ->
    go to project folder and run,
        npm test

        then script will call all API


 Run in Docker ->

    docker-compose build (.env setupted to docker mongo db)

    docker-compose up


    

        