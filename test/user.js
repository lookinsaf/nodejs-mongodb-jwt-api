const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
chai.use(chaiHttp);

const app = require("../app");

var user_name="Charlie"
var user_password="123456788"
var user_full_name="Charlie Chaplin"

//USER REGISTRAION API TEST CASE
describe("POST /register", () => {
  it("New User Registration API ", async () => {
    let res = await chai.request(app).post("/register").send({
      userName: user_name,
      password: user_password,
      fullName: user_full_name,
    });

    expect(res.status).to.equal(200);
  });
});

//USER LOGIN API
describe("POST /login", () => {
  it("User Login API ", async () => {
    let res = await chai.request(app).post("/login").send({
      userName: user_name,
      password: user_password,
    });
    expect(res.status).to.equal(200);
    expect(res.body.userName).to.equal(user_name);
  });
});

//PROFILE API TEST CASE
describe("GET /profile", () => {
  var token = null;
  beforeEach(async () => {
    let res = await chai.request(app).post("/login").send({
      userName: user_name,
      password: user_password,
    });
    
    expect(res.status).to.equal(200);
    token = res.body.token;
  });

  it("User Profile ", async () => {
    let res = await chai
      .request(app)
      .get("/profile")
      .set("Authorization", token)

    expect(res.status).to.equal(200);
  });
});
//USER LOGOUT

describe("GET /logout", () => {
  var token = null;
  beforeEach(async () => {
    let res = await chai.request(app).post("/login").send({
      userName: user_name,
      password: user_password,
    });
    
    expect(res.status).to.equal(200);
    token = res.body.token;
  });

  it("User Logout ", async () => {
    let res = await chai
      .request(app)
      .post("/logout")
      .set("Authorization", token)

    expect(res.status).to.equal(200);
  });
});
