var jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  const token = req.header("Authorization");
  if (!token)
    return res
      .status(401)
      .json({ error: "Access Denied", msg: "auth token not found" })

  try {
    const beare = token.split(" ");
    const result = jwt.verify(beare[1], process.env.JWT_TOKEN_SECRET);
    req.user=result
    next()
  } catch (error) {
    res.status(400).json({ error: "Access Denied", msg: "Invalied Token" });
  }
};
