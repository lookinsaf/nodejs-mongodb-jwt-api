const mongoos = require("mongoose");
var uniqueValidator = require('mongoose-unique-validator');

const userSchema = mongoos.Schema({
  userName: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
  },
  fullName: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  is_logged: {
    type: Number,
    default: 0,
  },
});
userSchema.plugin(uniqueValidator);

module.exports = mongoos.model("users", userSchema);
