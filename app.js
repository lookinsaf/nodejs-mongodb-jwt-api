const express = require("express");
const mongoose = require("mongoose");
var bodyParser = require('body-parser')
const swaggerUi = require('swagger-ui-express');
var YAML = require("yamljs");
var swaggerDocument = YAML.load("./swagger.yaml");
require("dotenv").config();

const app = express();
const port = process.env.APP_PORT;
//Swagger OPTION
var options = {
  swaggerOptions: {
      authAction: {
          JWT:
          {
              name: "JWT",
              schema: {
                  type: "apiKey",
                  in: "header",
                  name: "Authorization",
                  description: ""
              },
              value: "Bearer <JWT>"
          }
      }
  }
};

//Import routers
const userRoute = require("./routes/user");

app.use(bodyParser.json())
app.use("/", userRoute);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));

//Connect To MonogoDB
mongoose.connect(
  process.env.DB_CONNECTION,
  { useNewUrlParser: true, useUnifiedTopology: true ,'useCreateIndex':true},
  () => {
    console.log("MongoDB successfully Connected .");
  }
);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

module.exports = app;
