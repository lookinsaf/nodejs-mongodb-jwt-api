const express = require("express");
const router = express.Router();
const User = require("../models/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const verifyToken=require('../jwt/verify')

router.get("/", (req, res) => {
  res.send("This Is My Task");
});

//Login Route
router.post("/login", async (req, res) => {
  const user = await User.findOne({ userName: req.body.userName });
  if (!user) return res.status(400).json({ error: "Username Not found" });

  const valiedPassword = await bcrypt.compare(req.body.password, user.password);
  if (!valiedPassword)
    return res.status(400).json({ error: "Inavalied Passwor" });

  //Genarate JWT Token
  const token = jwt.sign(
    {
      _id: user._id,
    },
    process.env.JWT_TOKEN_SECRET,
    { expiresIn: 60 * 60 }
  );

  const updated=await User.findOneAndUpdate({ _id: user._id},{is_logged : 1 },{useFindAndModify: false})
  res.header('Authorization',token).status(200).json({userName:user.userName,token:token});
});

//Logout Route
router.post("/logout",verifyToken, async(req, res) => {
  const user = await User.findOne({ _id: req.user._id ,is_logged:1});
  if(user){
    const updated=await User.findOneAndUpdate({ _id: user._id},{is_logged : 0 },{useFindAndModify: false})
    res.status(200).send({"msg":"User Suscessfully Logedout"});
  }else{
    return res.status(400).json({ error: "Logout Error" });
  }

  
});

//Register Route
router.post("/register", async (req, res) => {
  //Genarate Hash Password
  const salt = bcrypt.genSaltSync(10);
  const hashPassword = await bcrypt.hash(req.body.password, salt);

  const user = new User({
    userName: req.body.userName,
    password: hashPassword,
    fullName: req.body.fullName,
  });

  try {
    const savedUser = await user.save();
    res.json({"_id":savedUser._id,"userName":user.userName});
  } catch (error) {
    res.json(error);
  }
});

//Profile
router.get("/profile", verifyToken,async (req, res) => {
  const user = await User.findOne({ _id: req.user._id ,is_logged:1});
  if(user) {
    res.send(user);
  }else{
    res.json({ error: "User Not Logged In" });;
  }
 
  
});

module.exports = router;
